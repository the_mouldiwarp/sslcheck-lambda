package main

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net"
	"regexp"
	"strconv"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/tidwall/gjson"
)

func main() {
	lambda.Start(Handler)
}

type ssldetail struct {
	Hostname   string   `json:"hostname"`
	Issuer     string   `json:"issuer"`
	NotBefore  string   `json:"notbefore"`
	NotAfter   string   `json:"notafter"`
	ExpiresIn  string   `json:"ExpiresIn"`
	DomainList []string `json:"domainlist"`
}

func sslconn(s string) *tls.Conn {
	conf := &tls.Config{InsecureSkipVerify: true}
	DefaultTimeout := 200 * time.Millisecond

	conn, err := tls.DialWithDialer(&net.Dialer{Timeout: DefaultTimeout}, "tcp", s, conf)
	if err != nil {
		panic("Failed to open tcp connection to target domain:port")
	}

	defer conn.Close()
	return conn
}

// FixMissingPort deals with presence or absence of a port in the request endpoint query string
func FixMissingPort(s string) string {
	type ep struct {
		domain string
		port   string
	}
	ret := ep{"", ""}
	rx := regexp.MustCompile(`(^[\w\.-]+)[:]?([\d]+)?$`)
	rxscan := rx.FindStringSubmatch(s)

	if len(rxscan) == 3 {
		ret.domain = rxscan[1]
		if rxscan[2] != "" {
			ret.port = rxscan[2]
		} else {
			ret.port = "443"
		}
		return fmt.Sprintf("%s:%s", ret.domain, ret.port)
	}
	return s
}

// ExpiresIn takes an expiry time for a cert and compares it to current system time returning a string
// representaiton of time remaining on the certificate in days.
func ExpiresIn(de time.Time) string {
	dnow := time.Now()
	diffhours := de.Sub(dnow).Hours()
	diffdays := diffhours / float64(24)
	return strconv.FormatFloat(diffdays, 'f', 0, 64)
}

// GetIssuer returns the notAfter time object as a time object
func getIssuer(tc *tls.Conn) string {
	return tc.ConnectionState().PeerCertificates[0].Issuer.CommonName
}

// NotAfter returns the notAfter time object as a time object
func notAfter(tc *tls.Conn) time.Time {
	return tc.ConnectionState().PeerCertificates[0].NotAfter
}

// NotBefore returns the notAfter time object as a time object
func notBefore(tc *tls.Conn) time.Time {
	return tc.ConnectionState().PeerCertificates[0].NotBefore
}

// DomainList lists all SAN entries for the site
func domainList(tc *tls.Conn) []string {
	return tc.ConnectionState().PeerCertificates[0].DNSNames
}

// GetDetails combines the methods above to generate the reponse string for the API call
func getDetails(s string) ssldetail {
	d := FixMissingPort(s)
	tc := sslconn(d)
	return ssldetail{
		Hostname:   d,
		Issuer:     getIssuer(tc),
		NotBefore:  notBefore(tc).String(),
		NotAfter:   notAfter(tc).String(),
		ExpiresIn:  ExpiresIn(notAfter(tc)),
		DomainList: domainList(tc)}
}

// Handler used to execute the code for the Lambda
func Handler(ctx context.Context, evt json.RawMessage) (events.APIGatewayProxyResponse, error) {
	param := gjson.Get(string(evt), "domain").String()
	result := getDetails(param)
	b, _ := json.Marshal(result)
	returnVal := events.APIGatewayProxyResponse{
		Body:       "",
		StatusCode: 200,
	}
	returnVal.Body = string(b)
	return returnVal, nil
}
