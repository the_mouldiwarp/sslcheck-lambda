.PHONY:compile
.PHONY:run
.PHONY:ship
.PHONY:test
test:
	@echo "Performing codebase tests"
	@go test -cover
compile: test
	@echo "Compiling lambda"
	@GOOS=linux go build -o sslcheck
run:	compile
	@echo "Launching lambda with AWS 'sam' cli"
	@AWS_DEFAULT_REGION=us-west-2 sam local invoke "sslcheck" -e event.json
ship:	compile
	@echo "Creating lambda zip file"
	@zip sslcheck-lambda.zip sslcheck