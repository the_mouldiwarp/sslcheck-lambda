package main

import (
	"testing"
	"time"
)

func TestFixMissingPort(t *testing.T) {

	list := []struct {
		test   string
		expect string
	}{
		{"www.microsoft.com", "www.microsoft.com:443"},
		{"www.microsoft.com:8443", "www.microsoft.com:8443"},
		{"www.hyphen-in-domain-name.com", "www.hyphen-in-domain-name.com:443"},
		{"www.multi.domain.name.com", "www.multi.domain.name.com:443"},
	}

	for _, test := range list {
		got := FixMissingPort(test.test)
		expect := test.expect
		if got != expect {
			t.Errorf("got %s; want %s", got, expect)
		}
	}
}

func TestExpiresIn(t *testing.T) {
	now := time.Now()
	then := now.AddDate(0, 0, -25)
	expect := "-25"

	got := ExpiresIn(then)
	if got != expect {
		t.Errorf("got %s; expect %s", got, expect)
	}

}
