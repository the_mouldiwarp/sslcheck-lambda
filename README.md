# L&D development of a lambda version of the sslcheck utility

## About

The sslcheck utility started life as a ruby gem, was ported to golang, used as a vehicle to create a web api and finally updated to include SSL and command line utility capability.

It checks a domain's SSL configuration returning infirmation on the signing authority, valid from, valid to and expires in 'n' days and SAN listings.

For example:

``` txt
Hostname:   www.skybingo.com:443
Issuer:     COMODO RSA Extended Validation Secure Server CA
Not Before: 2018-06-19 00:00:00 +0000 UTC
Not After:  2019-06-19 23:59:59 +0000 UTC
Expires in: 153 Days
SAN List:
            www.skybingo.com
            online.skybingo.com
            skybingo.com
            support.skybingo.com
```

In its Lamba incarnation it accepts requests with data in the form of:

``` json
{
  "domain": "www.microsoft.com:443"
}
```

If no port is provided then it defaults to 443, alternate ports (8443 for example) are supported.

## Local development

This version uses local development tools, provided by Amazon, which simulates the execution of a lambda using a docker container (as happens within the AWS platform)

To execute a local test, the following environment set-up is required.

### SAM local

Quote: `AWS Lambda functions are awesome, at the same time developing Lambda functions can be frustrating, especially when dealing with a compiled language such as Go (there is currently no web interface to write the code inline). Every code change requires you to compile, zip and upload the code to test it. That’s where aws-sam-local comes in. You can simply make a code change and type make run in the terminal to see the results.`

Install the [SAM-local-cli](https://github.com/awslabs/aws-sam-cli)

## Local execution

This is done using a [Makefile](./Makefile) with 3 targets:

|Target|Dependency|Description|
|:---|:---|:---|
|test|-|Performs codebase testing|
|compile|test|Creates the golang executable for a linux based platform|
|run|compile|executes the Lambda using sample data via the sam cli|
|ship|compile|Creates a zip file of the lambda ready for deplyment artifactory or aws|

## dependencies

[template.yml](./template.yml)

This sets out the environment in which the lambda executes.  Similar to the configuratio required in AWS for execution:

``` yml
AWSTemplateFormatVersion: '2010-09-09'
Transform: AWS::Serverless-2016-10-31
Description: >
  L&D

  Learning golang Lambda

# More info about Globals: https://github.com/awslabs/serverless-application-model/blob/master/docs/globals.rst
Globals:
  Function:
    Timeout: 2

Resources:
  myapp:
    Type: AWS::Serverless::Function # More info about Function Resource: https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md#awsserverlessfunction
    Properties:
      CodeUri: .
      Handler: myapp
      Runtime: go1.x
      Environment: # More info about Env Vars: https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md#environment-object
        Variables:
          SECRET: "35c760f4-b3dc-4657-b4f3-2c6566d4f42e"
```

[event.json](./event.json)

This is the sample data as would otherwise be submitted as part of the request to the AWS application gateway and subsequently to the lambda

``` json
{
  "domain": "www.skybingo.com:443"
}
```

## Example execution

```╰─➤  make run
go test -cover
PASS
coverage: 40.0% of statements
ok      bitbucket.org/the_mouldiwarp/sslcheck-lambda    0.009s
GOOS=linux go build -o sslcheck
AWS_DEFAULT_REGION=us-west-2 sam local invoke "sslcheck" -e event.json
2019-01-18 12:16:31 Invoking myapp (go1.x)

Fetching lambci/lambda:go1.x Docker container image......
2019-01-18 11:05:53 Mounting /Users/gma34/go/src/bitbucket.org/the_mouldiwarp/sslcheck-lambda as /var/task:ro inside runtime container
START RequestId: 07ca42c1-4b93-12db-2866-c8d7549ec47c Version: $LATEST
END RequestId: 07ca42c1-4b93-12db-2866-c8d7549ec47c
REPORT RequestId: 07ca42c1-4b93-12db-2866-c8d7549ec47c      Duration: 199.60 ms     Billed Duration: 200 ms Memory Size: 128 MB     Max Memory Used: 7 MB
```

``` json
{
  "statusCode": 200,
  "headers": null,
  "body": "{\"hostname\":\"www.microsoft.com:443\",\"issuer\":\"Microsoft IT TLS CA 4\",\"notbefore\":\"2018-01-16 21:24:02 +0000 UTC\",\"notafter\":\"2020-01-16 21:24:02 +0000 UTC\",\"ExpiresIn\":\"363\",\"domainlist\":[\"privacy.microsoft.com\",\"c.s-microsoft.com\",\"microsoft.com\",\"i.s-microsoft.com\",\"staticview.microsoft.com\",\"www.microsoft.com\",\"wwwqa.microsoft.com\"]}"
}
```

## Upload to AWS (manual terraform)

This is not an exercise in terraform, but instead an exercise in the createn, development and execution of lambda's on the AWS platform.